import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html',
  providers: [
    InAppBrowser
  ]
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private inAppBrowser: InAppBrowser) {
   this.platform = platform;
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.show();
      this.openWebpage("https://eazy.mazars.fr");
      this.exitApp();
    });
    
  }
  
  exitApp(){
    this.platform.exitApp();
 }
  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: 'no',
     // _blank: 'yes',
      //location: 'no',
      //hideurlbar: 'yes',
      toolbar: 'no'
    }
    this.inAppBrowser.create(url, '_system', options);
    
  }
}

